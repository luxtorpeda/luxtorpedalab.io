quake-campaign-soundtrack.tar.xz

  Steam version of Quake 1 comes with broken soundtrack;
  these files fix this problem.

  from: https://steamcommunity.com/sharedfiles/filedetails/?id=119489135
