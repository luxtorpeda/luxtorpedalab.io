dkpatch.zip - Hexen: Deathkings of the Dark Citadel upgrade

These files should be unzipped into your current Hexen: Deathkings of the Dark Citadel directory.

To apply the patch, type "patch" from the DOS prompt.

Fixes:

* Redbook audio now plays correctly.

from: https://www.doomworld.com/idgames/idstuff/hexen/dkpatch
