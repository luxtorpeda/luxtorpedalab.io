EULA.txt

  Id EULA for patch files

  from: https://ioquake3.org/extras/patch-data/


quake3-latest-pk3s.zip

  The latest patch files for Q3A and Q3TA

  from: https://ioquake3.org/extras/patch-data/


quake3ta-latest-pk3s.tar.xz

  Subset of files from quake3-latest-pk3s.zip that are missing from
  Steam installation of Quake 3: Team Arena, repackaged in tar.xz.

  These files are required to play Q3TA using ioquake3; Luxtorpeda
  will download and install these files automatically.
