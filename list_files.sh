#!/bin/bash

pushd public > /dev/null || exit 1

# calculate checksums
#
find files -type d | while read -r dirname ; do
	pushd "$dirname" > /dev/null || exit 1
	sha256sum ./* 2> /dev/null > checksum.sha256
	popd > /dev/null || exit 1
done

# remove empty files
#
find files -type f -empty -delete

# generate index pages
#
find files -type d | while read -r dirname ; do
	tree \
		-H "/$dirname" \
		-I 'index.html' \
		-T "Directory Tree: $dirname" \
		--charset utf-8 \
		--noreport \
		"$dirname" > "$dirname/index.html"
done

popd > /dev/null || exit 1
