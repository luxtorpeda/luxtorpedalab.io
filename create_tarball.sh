#!/bin/bash

# Package content of dist/ subdirectory in reproducible way.

set -e

# shellcheck disable=SC2155
export SOURCE_DATE_EPOCH=$(git log -1 --format=%ct)

list_dist () {
	{
		find dist -type f
		find dist -type l
	} | sort
}

create_dist_tar_xz () {
	tar \
		--format=v7 \
		--mode='a+rwX,o-w' \
		--owner=0 \
		--group=0 \
		--mtime="@$SOURCE_DATE_EPOCH" \
		-cf dist.tar \
		--files-from=-
	xz dist.tar
}

set -x

list_dist | create_dist_tar_xz
